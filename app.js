const app = Vue.createApp({
    data() {
        return {
            apiKey: '3490bf7f634147ec9e05945785c89e32',
            apiUrl: "",
            viewDetails: false,
            newsData: [],
            category: "business",
            country: "ph",
            searchtext: "",
            maxPerPage: 12,
            currentPage: 1,
            totalResults: 0,
            countries: "",
            categories: "",
            searchbtn: "",
        }
    },
    methods: {
        handleFilters(event) {
            console.log(event)
            // this.fetchNewsFeed(this.country, this.category, search);
        },

        handleViewDetails(index, item) {
            // console.log(item, index)
            this.postDetails = item
            console.log(this.postDetails.title)
            if(this.postDetails) {
                this.viewDetails = true
            }
        },
        handleCloseDetails() {
            this.viewDetails = false
        },
        resetData() {
            this.currentPage = 1;
            this.newsData = [];
        },
        clearData() {
            this.currentPage = 1;
            this.searchtext = ''
            this.countries = ''
            this.categories = ''
            this.newsData = [];
            this.fetchTopNews()
        },
        fetchSearchNews() {
            if(this.searchword !== '' && this.countries === '' && this.categories === '')
            {
              this.apiUrl = 'https://newsapi.org/v2/everything?q=' + this.searchtext +
                            '&pageSize=' + this.maxPerPage +
                            '&apiKey=' + this.apiKey;
              this.isBusy = true;
              this.resetData();
              this.fetchNewsFeed();
            } else if(this.countries !== '' && this.categories !== '' && this.searchtext === ''){
                this.apiUrl = 'https://newsapi.org/v2/top-headlines?country=' + this.countries +
                            '&category=' + this.categories +
                            '&pageSize=' + this.maxPerPage +
                            '&apiKey=' + this.apiKey;
                this.isBusy = true;
                this.resetData();
                this.fetchNewsFeed();
            } else {
              this.fetchTopNews();
            }
          },
          fetchTopNews() {
            this.apiUrl = 'https://newsapi.org/v2/top-headlines?country=' + this.country +
                            '&category=' + this.category +
                            '&pageSize=' + this.maxPerPage +
                            '&apiKey=' + this.apiKey;
            // this.isBusy = true;
            // this.searchword = '';
            
            this.resetData();
            this.fetchNewsFeed();
        },
        
        fetchNewsFeed() {
            let req  = new Request(this.apiUrl + '&page=' + this.currentPage);
            fetch(req)
            .then((resp) => resp.json())
            .then((data) => {
                this.totalResults = data.totalResults;
                data.articles.forEach(element => {
                console.log(this.newsData)
                this.newsData.push(element);
                });
                this.isBusy = false;
                this.showloader = false;
            })
            .catch((error) => {
                console.log(error);
            })
        },
        scrollTrigger() {
            const observer = new IntersectionObserver((entries) => {
              entries.forEach(entry => {
                if(entry.intersectionRatio > 0 && this.currentPage < this.pageCount) {
                  this.showloader = true;
                  this.currentPage += 1;
                  this.fetchNewsFeed();
                }
              });
            });
            observer.observe(this.$refs.infinitescrolltrigger);
        }
    },
    created() {
        this.fetchTopNews();
    },
    computed: {
        theDate() {
            return moment(this.newsData.publishedAt).format('MMM Do YYYY');
        },
        pageCount() {
            return Math.ceil(this.totalResults/this.maxPerPage);
        }
    },
    mounted() {
        this.scrollTrigger();
    }
})

app.mount('#app')